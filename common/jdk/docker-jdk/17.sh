#!/bin/sh

set -e

echo " "
echo " "
echo 'build docker-jdk-17 start'

# 17-jdk-focal-docker
docker build \
    -t docker-jdk:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/jdk/docker-jdk/17/Dockerfile . --no-cache
docker image tag docker-jdk:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/docker-jdk:17
docker push ${CI_REGISTRY}/opcal-project/containers/docker-jdk:17

echo 'build docker-jdk-17 finished'
echo " "
echo " "
