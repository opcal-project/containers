#!/bin/sh

set -e

echo " "
echo " "
echo 'build graalvm-java11-22 start'

GRL_VERSION=22.0.0.2.r11-grl
BASE_IMAGE=registry.gitlab.com/opcal-project/containers/ubuntu:focal

# graalvm-java11-22
docker build \
    --build-arg BASE_IMAGE=${BASE_IMAGE} \
    --build-arg GRL_VERSION=${GRL_VERSION} \
    -t graalvm:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/jdk/graalvm/base/Dockerfile . --no-cache
docker image tag graalvm:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/graalvm:java11-22-${TAG_VERSION}
docker image tag graalvm:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/graalvm:java11-22
docker push ${CI_REGISTRY}/opcal-project/containers/graalvm:java11-22-${TAG_VERSION}
docker push ${CI_REGISTRY}/opcal-project/containers/graalvm:java11-22

echo 'build graalvm-java11-22 finished'
echo " "
echo " "