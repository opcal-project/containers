#!/bin/sh

set -e

echo " "
echo " "
echo 'build eclipse-temurin-11-jdk-focal start'

IMAGE=eclipse-temurin:11-jdk-focal

# 11-jdk-focal
docker build \
    --build-arg BASE_IMAGE=${IMAGE} \
    -t eclipse-temurin:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/jdk/eclipse-temurin/base/ubuntu/Dockerfile . --no-cache
docker image tag eclipse-temurin:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jdk-focal-${TAG_VERSION}
docker image tag eclipse-temurin:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jdk-focal
docker push ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jdk-focal-${TAG_VERSION}
docker push ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jdk-focal

echo 'build eclipse-temurin-11-jdk-focal finished'
echo " "
echo " "