#!/bin/sh

set -e

echo " "
echo " "
echo 'build liberica-nik-java17-22 start'

NIK_VERSION=22.0.0.2.r17-nik
BASE_IMAGE=registry.gitlab.com/opcal-project/containers/ubuntu:focal

# liberica-nik-java17-22
docker build \
    --build-arg BASE_IMAGE=${BASE_IMAGE} \
    --build-arg NIK_VERSION=${NIK_VERSION} \
    -t liberica-nik:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/jdk/liberica-nik/base/Dockerfile . --no-cache
docker image tag liberica-nik:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/liberica-nik:java17-22-${TAG_VERSION}
docker image tag liberica-nik:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/liberica-nik:java17-22
docker push ${CI_REGISTRY}/opcal-project/containers/liberica-nik:java17-22-${TAG_VERSION}
docker push ${CI_REGISTRY}/opcal-project/containers/liberica-nik:java17-22

echo 'build liberica-nik-java17-22 finished'
echo " "
echo " "