#!/bin/sh

set -e

echo " "
echo " "
echo 'build ubuntu:jammy start'

BASE_IMAGE=ubuntu:jammy

# ubuntu:jammy
docker build \
    --build-arg BASE_IMAGE=${BASE_IMAGE} \
    -t ubuntu:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/linux/ubuntu/base/Dockerfile . --no-cache
docker image tag ubuntu:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/ubuntu:jammy-${TIMESTAMP}
docker image tag ubuntu:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/ubuntu:jammy
docker push ${CI_REGISTRY}/opcal-project/containers/ubuntu:jammy-${TIMESTAMP}
docker push ${CI_REGISTRY}/opcal-project/containers/ubuntu:jammy

echo 'build ubuntu:jammy finished'
echo " "
echo " "