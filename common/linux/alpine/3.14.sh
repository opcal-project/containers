#!/bin/sh

set -e

echo " "
echo " "
echo 'build alpine:3.14 start'

BASE_IMAGE=alpine:3.14
GOSU_VERSION=$(curl https://api.github.com/repos/tianon/gosu/releases/latest | grep tag_name | cut -d '"' -f 4)

# alpine:3.14
docker build \
    --build-arg BASE_IMAGE=${BASE_IMAGE} \
    --build-arg GOSU_VERSION=${GOSU_VERSION} \
    -t alpine:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/linux/alpine/base/Dockerfile . --no-cache
docker image tag alpine:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/alpine:3.14-${TIMESTAMP}
docker image tag alpine:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/alpine:3.14
docker push ${CI_REGISTRY}/opcal-project/containers/alpine:3.14-${TIMESTAMP}
docker push ${CI_REGISTRY}/opcal-project/containers/alpine:3.14

echo 'build alpine:3.14 finished'
echo " "
echo " "