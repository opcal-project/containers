#!/bin/sh

set -e

echo " "
echo " "
echo 'build eclipse-temurin-11-jre-focal start'

IMAGE=eclipse-temurin:11-jre-focal

# 11-jre-focal
docker build \
    --build-arg BASE_IMAGE=${IMAGE} \
    -t eclipse-temurin:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/jre/eclipse-temurin/base/ubuntu/Dockerfile . --no-cache
docker image tag eclipse-temurin:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jre-focal-${TAG_VERSION}
docker image tag eclipse-temurin:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jre-focal
docker push ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jre-focal-${TAG_VERSION}
docker push ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jre-focal

echo 'build eclipse-temurin-11-jre-focal finished'
echo " "
echo " "