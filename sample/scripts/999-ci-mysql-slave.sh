#!/bin/sh

set -e

echo " "
echo " "
echo 'build ci-mysql-slave start'

docker build -t mysql-slave:${TAG_VERSION} -f ${CI_PROJECT_DIR}/sample/mysql/master-slave/Dockerfile.slave . --no-cache
docker image tag mysql-slave:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-slave:${PROJECT_VERSION}-${TIMESTAMP}
docker image tag mysql-slave:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-slave:${LATEST_VERSION}
docker image tag mysql-slave:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-slave:latest
docker push ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-slave:${PROJECT_VERSION}-${TIMESTAMP}
docker push ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-slave:${LATEST_VERSION}
docker push ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-slave:latest


echo 'build ci-mysql-slave finished'
echo " "
echo " "