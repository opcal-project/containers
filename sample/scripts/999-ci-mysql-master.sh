#!/bin/sh

set -e

echo " "
echo " "
echo 'build ci-mysql-master start'

docker build -t mysql-master:${TAG_VERSION} -f ${CI_PROJECT_DIR}/sample/mysql/master-slave/Dockerfile.master . --no-cache
docker image tag mysql-master:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-master:${PROJECT_VERSION}-${TIMESTAMP}
docker image tag mysql-master:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-master:${LATEST_VERSION}
docker image tag mysql-master:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-master:latest
docker push ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-master:${PROJECT_VERSION}-${TIMESTAMP}
docker push ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-master:${LATEST_VERSION}
docker push ${CI_REGISTRY}/opcal-project/containers/mysql/mysql-master:latest

echo 'build ci-mysql-master finished'
echo " "
echo " "